package stringTokens;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith( JUnitParamsRunner.class )
public class StringTokensUnitTest {

    @Test
    @Parameters( {
                         "._!@test!!!, test!!!",
                         "He is a very very good boy_ isn't he?, He is a very very good boy_ isn't he?"
                 } )
    public void trimFromBegin( String input, String expectedOutput ) {
        assertEquals( expectedOutput, StringTokens.trimFromBegin( input ) );
    }

    @Test
    public void trimFromBeginOnlyJunk() {
        String input = ",._!@";
        String expectedOutput = "";
        assertEquals( expectedOutput, StringTokens.trimFromBegin( input ) );
    }

    @Test
    public void trimFromBeginEmptyString() {
        String input = "";
        String expectedOutput = "";
        assertEquals( expectedOutput, StringTokens.trimFromBegin( input ) );
        input = "           ";
        assertEquals( expectedOutput, StringTokens.trimFromBegin( input ) );
    }

    @Test
    public void trimFromEnd() {
        String input = "test!!_,.!!";
        String expectedOutput = "test";
        assertEquals( expectedOutput, StringTokens.trimFromEnd( input ) );
        input = "           ";
        expectedOutput = "";
        assertEquals( expectedOutput, StringTokens.trimFromEnd( input ) );
    }

    @Test
    public void trimFromEndOnlyJunk() {
        String input = "@_,.!!";
        String expectedOutput = "";
        assertEquals( expectedOutput, StringTokens.trimFromEnd( input ) );
    }

    @Test
    public void trimFromEndClassic() {
        String input = "He is a very very good boy, isn't he?";
        String expectedOutput = "He is a very very good boy, isn't he";
        assertEquals( expectedOutput, StringTokens.trimFromEnd( input ) );
    }

    @Test
    public void trimFromEndEmptyString() {
        String input = "";
        String expectedOutput = "";
        assertEquals( expectedOutput, StringTokens.trimFromEnd( input ) );
    }

    @Test
    public void trimNonLetterCharacters() {
        String input = ",.!@hbbcnbn,._";
        String expectedOutput = "hbbcnbn";
        assertEquals( expectedOutput, StringTokens.trimNonLetterCharacters( input ) );
    }

    @Test
    public void trimNonLetterCharactersClassic() {
        String input = "He is a very very good boy, isn't he?";
        String expectedOutput = "He is a very very good boy, isn't he";
        assertEquals( expectedOutput, StringTokens.trimNonLetterCharacters( input ) );
    }

    @Test
    public void trimNonLetterCharactersOnlyJunk() {
        String input = ",.!@,._";
        String expectedOutput = "";
        assertEquals( expectedOutput, StringTokens.trimNonLetterCharacters( input ) );
    }

    @Test
    public void trimNonLetterCharactersEmptyString() {
        String input = "";
        String expectedOutput = "";
        assertEquals( expectedOutput, StringTokens.trimNonLetterCharacters( input ) );
    }

    @Test
    public void filterNonLetters() {
        String input = "gn@gh_nm'j, rt";
        String expectedOutput = "gn gh nm j  rt";
        assertEquals( expectedOutput, StringTokens.replaceNonLetterCharactersWithSpaces( input ) );
    }

    @Test
    @Parameters( {
                         "._!@test!!!, test",
                         "He is a very very good boy_ isn't he?, He is a very very good boy  isn t he",
                         "gn@gh_nm'j! rt, gn gh nm j  rt"
                 } )
    public void filterNonLetters( String input, String expectedOutput ) {
        assertEquals( expectedOutput, StringTokens.replaceNonLetterCharactersWithSpaces( input ) );
    }

    @Test
    @Parameters( {
                         "euryer   kdh, euryer kdh",
                         "      k, k",
                         "k             , k",
                         "!@_sdskjdh'''sdjkhs_ jkdh, sdskjdh sdjkhs jkdh",
                         "Just regular TEXT, Just regular TEXT",
                         ","
                 } )
    public void normalizeSpaces( String input, String expectedOutput ) {
        assertEquals( expectedOutput, StringTokens.normalizeSpaces( input ) );
    }

    @Test
    public void normalizeSpaces() {
        String input = "           ";
        String expectedOutput = "";
        assertEquals( expectedOutput, StringTokens.normalizeSpaces( input ) );

        input = "      ,     ";
        assertEquals( expectedOutput, StringTokens.normalizeSpaces( input ) );
    }

}
