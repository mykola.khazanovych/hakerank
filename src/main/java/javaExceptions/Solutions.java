package javaExceptions;

import java.util.Scanner;

/**
 * This class was introduced as it is in: https://www.hackerrank.com/challenges/java-exception-handling/
 */
public class Solutions {

    public static void main( String[] args ) {
        try( Scanner in = new Scanner( System.in ) ){
            while ( in.hasNextInt() ) {
                int n = in.nextInt();
                int p = in.nextInt();
                try {
                    System.out.println( new MyCalculator().power( n, p ) );
                } catch ( Exception e ) {
                    System.out.println( e );
                }
            }
        }
    }
}

class MyCalculator {
    long power( int base, int power ) throws Exception {
        if( base < 0 || power < 0 ){
            throw new Exception( "n or p should not be negative." );
        } else if ( base == 0 && power == 0 ) {
            throw new Exception( "n and p should not be zero." );
        }
        return (long) Math.pow( base, power );
    }
}
