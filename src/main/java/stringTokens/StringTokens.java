package stringTokens;

import java.util.Scanner;

/**
 * Given a string, matching the regular expression [A-Za-z !,?._'@]+, split the string into tokens.
 * We define a token to be one or more consecutive English alphabetic letters.
 * Then, print the number of tokens, followed by each token on a new line.
 */
public class StringTokens {

    public static void main( String[] args ) {
        printNormalizedString(
                normalizeSpaces(
                        getLine()
                )
        );
    }

    private static String getLine() {
        try ( Scanner scanner = new Scanner( System.in ) ) {
            return scanner.nextLine();
        }
    }

    static String normalizeSpaces( String input ) {
        String filteredString = replaceNonLetterCharactersWithSpaces( input );
        boolean oneAllowedSpaceSeparator = true;
        StringBuilder normalized = new StringBuilder();
        for ( String symbol : filteredString.split( "" ) ) {
            if ( " ".equalsIgnoreCase( symbol ) && oneAllowedSpaceSeparator ) {
                normalized.append( symbol );
                oneAllowedSpaceSeparator = false;
            }
            if ( symbol.equalsIgnoreCase( " " ) && !oneAllowedSpaceSeparator ) {
                continue;
            }
            oneAllowedSpaceSeparator = true;
            normalized.append( symbol );
        }
        return normalized.toString();
    }

    static String replaceNonLetterCharactersWithSpaces( String input ) {
        String trimmed = trimNonLetterCharacters( input );
        StringBuilder builder = new StringBuilder();
        for ( String str : trimmed.split( "" ) ) {
            if ( isJunk( str ) ) {
                builder.append( " " );
                continue;
            }
            builder.append( str );
        }
        return builder.toString();
    }

    static String trimNonLetterCharacters( String rawInputString ) {
        return trimFromEnd( trimFromBegin( rawInputString ) )
                .trim();
    }

    static String trimFromBegin( String rawInputString ) {
        int beginCounter = 0;
        for ( String symbol : rawInputString.split( "" ) ) {
            if ( isJunk( symbol ) ) {
                beginCounter++;
                continue;
            }
            break;
        }
        return rawInputString.substring( beginCounter )
                             .trim();
    }

    static String trimFromEnd( String trimmedFromBegin ) {
        int length = trimmedFromBegin.length();
        int counter = 0;
        for ( int i = length - 1; i >= 0; i-- ) {
            if ( !isJunk( String.valueOf( trimmedFromBegin.charAt( i ) ) ) ) {
                break;
            }
            counter++;
        }
        return trimmedFromBegin.substring( 0, length - counter ).trim();
    }

    private static void printNormalizedString( String filtered ) {
        if ( filtered.length() == 0 ) {
            System.out.println( "0" );
        } else {
            String[] arr = filtered.split( " " );
            System.out.println( arr.length );
            for ( String str : arr ) {
                System.out.println( str );
            }
        }
    }

    private static boolean isJunk( String str ) {
        return str.equals( "!" )
                || str.equals( "," )
                || str.equals( "?" )
                || str.equals( "." )
                || str.equals( "'" )
                || str.equals( "_" )
                || str.equals( "@" );
    }
}
