package javaList;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//Problem statement https://www.hackerrank.com/rest/contests/master/challenges/java-list/download_pdf?language=English
public class Solution {

    public static void main( String[] args ) {
        processInput();
    }

    private static void processInput() {
        try ( Scanner scanner = new Scanner( System.in ) ) {
            int quantityOfElementInList = scanner.nextInt();
            List<Integer> listWithElements = getListWithElements( scanner, quantityOfElementInList );
            int numberOfQueries = scanner.nextInt();
            parseAndRunQueriesOnList( scanner, listWithElements, numberOfQueries );
            printListAsLine( listWithElements );
        }
    }

    private static List<Integer> getListWithElements( Scanner scanner, int elementsQuantityInList ) {
        List<Integer> list = new ArrayList<>( elementsQuantityInList );
        for ( int i = 0; i < elementsQuantityInList; i++ ) {
            list.add( scanner.nextInt() );
        }
        return list;
    }

    private static void parseAndRunQueriesOnList( Scanner scanner, List<Integer> list, int numberOfQueries ) {
        for ( int i = 0; i < numberOfQueries; i++ ) {
            String queryName = scanner.next();
            switch ( queryName ) {
                case "Insert":
                    int index = scanner.nextInt();
                    int value = scanner.nextInt();
                    list.add( index, value );
                    break;
                case "Delete":
                    index = scanner.nextInt();
                    list.remove( index );
                    break;
                default:
                    throw new IllegalArgumentException( "Unknown query command!" );
            }
        }
    }

    private static void printListAsLine( List<Integer> listWithElements ) {
        listWithElements.forEach( e -> System.out.print( e + " " ) );
    }
}
